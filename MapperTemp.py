from netCDF4 import Dataset
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import matplotlib.colorbar
import os
import cartopy.crs as crs
from cartopy.feature import NaturalEarthFeature
from wrf import (getvar, interplevel, to_np, latlon_coords, get_cartopy,
                 cartopy_xlim, cartopy_ylim)

def TempMap(filename, temp, cartVar, time, lats, lons, smooth):
	fig = plt.figure(figsize=(12,9))
	ax = plt.axes(projection=cartVar)
	
# Download and add the states and coastlines
	states = NaturalEarthFeature(category="cultural", scale="50m",facecolor="none",name="admin_1_states_provinces_shp")
	
	
	ax.add_feature(states, linewidth=1.5, edgecolor="black")

#	ax.set_extent(extent)
	ax.coastlines('50m', linewidth=1.8)
	ax.gridlines(color="black", linestyle="dotted")
	
	#Contours based on the individual Variable
	plt.contour(to_np(lons), to_np(lats), to_np(smooth), 10, colors="black",transform=crs.PlateCarree())
	plt.contourf(to_np(lons), to_np(lats), to_np(smooth), 10,transform=crs.PlateCarree(),cmap=get_cmap("YlOrRd"))
	
	#Color bar
	plt.colorbar(ax=ax, shrink=.98)
	
	
	ax.set_xlim(cartopy_xlim(temp))
	ax.set_ylim(cartopy_ylim(temp))
	
	plt.title("Temperature in K")
	a = input('Enter a file name with .png')
	
	
	plt.savefig(a)
	plt.show()
	
	
	plt.close()
	
