
To run

1. Copy wrfout file using 'cp /data/wrfout* /home/$USER/cloud9-wrf'
2. Edit Mainwrf.py placing the wrfout file in the file1 line
 e.g. vi MainWrf.py -> 'i' -> change wrfout data date
3. Make sure anaconda is activated utilizing 'conda activate wrfuser'
4. Run 'python MainWrf.py' and select number based on the graph inputs shown
5. Follow on screen prompts


Moving/Changing WRF files


