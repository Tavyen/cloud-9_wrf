from netCDF4 import Dataset
import numpy as np 

def newFile(time,lon,lat,var3D,var_vble,time_vble,var_site,fout):
	file = Dataset('tasmax_day_CCSM4_19890201_r6i1p1_19890201-19900131.nc4','w',format='NETCDF4')

	file.createDimension('time',len(time))
	ovar = file.createVariable('tasmax', 'f4', ('time'))
	otime = file.createVariable('time', 'f4', 'time')

	ovar.inits = var_vble.getncattr('units')
	ovar.long_names = var_vble.getncattr('long_name')
	ovar.standard_name = var_vble.getncattr('standard_name')
#	ovar.missing_value = var_vble.getncattr('missing_value')
#
	otime.units =  time_vble.getncattr('units')
	otime.calendar = time_vble.getncattr('calendar')
	otime.axis = time_vble.getncattr('axis')
	otime.long_name = time_vble.getncattr('long_name')
#	otime.standard_name = time_vble.getncattr('standard_name')
	ovar[:] = var_site 
	otime[:] = time 
#	file.close() 
	print("#############") 
	print("OUTFILE",fout)
	print("#############") 