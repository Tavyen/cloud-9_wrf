######For Utilizing Python to get variables from WRF

from netCDF4 import Dataset
import numpy as np
import importlib
import os
from netCDF4 import Dataset
from Near import nearidx
from newFile import newFile
from wrf import getvar, get_cartopy, latlon_coords, geo_bounds, smooth2d
from urllib.request import urlopen
from datetime import datetime, timedelta

fin = "wrfout_d01_2020-06-15_00:00:00"
file1 =  Dataset(fin, "r", format="NETCDF4")

#---get attributes from all the variables

time = getvar(file1, "times")
try:
	lon = getvar(file1, "lon")
	lat = getvar(file1, "lat")
except:
	lon = getvar(file1, "XLONG")
	lat = getvar(file1, "XLAT")
td = getvar(file1, "td")
temp = getvar(file1, "T2")
slp = getvar(file1, "slp")
rh = getvar(file1, "rh")
precip = getvar(file1, "precip")

def getXY(Var):
	lats, lons = latlon_coords(Var)
	return lats, lons
def getBounds(Var):
	bounds = geo_bounds(Var)
	return bounds

print(getXY(slp))
	

